# Generated by Django 2.2 on 2020-09-24 11:33

import api.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_auto_20200901_1743'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachment',
            name='file',
            field=models.FileField(upload_to=api.models.get_file_path),
        ),
        # migrations.CreateModel(
        #     name='Account_view',
        #     fields=[
        #         ('type', models.CharField(max_length=2)),
        #         ('sysid', models.CharField(max_length=64, primary_key=True, serialize=False)),
        #         ('tableid', models.IntegerField(max_length=32)),
        #         ('cause', models.CharField(max_length=64)),
        #         ('money', models.DecimalField(decimal_places=2, max_digits=10)),
        #         ('account_date', models.DateTimeField(auto_now_add=True, null=True)),
        #         ('finish_flag', models.IntegerField()),
        #         ('expense_name', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='api.UserInfo')),
        #         ('project_name', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='api.Project_info')),
        #     ],
        #     options={
        #         'db_table': 'account_view',
        #     },
        # ),
    ]
